docker run -d \
    --restart unless-stopped \
    --name phpmyadmin \
    --network my-bridge \
    -e PMA_HOST=mysql \
    -p 2021:80 \
    phpmyadmin/phpmyadmin
