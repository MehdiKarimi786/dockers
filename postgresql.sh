curDir=$(pwd)

docker run -d \
    --restart unless-stopped \
    --name postgresql \
    --network my-bridge \
    -e POSTGRES_DB="postgres-db" \
    -e POSTGRES_USER="postgres-user" \
    -e POSTGRES_PASSWORD="postgres-pass" \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
    -v $curDir/docker-volumes/postgres:/var/lib/postgresql/data \
    postgres
