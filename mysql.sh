curDir=$(pwd)

docker run -d \
    --restart unless-stopped \
    --name mysql \
    --network my-bridge \
    -e MYSQL_ROOT_PASSWORD="mysql-root-pass" \
    -e MYSQL_DATABASE="my-db" \
    -e MYSQL_USER="mysql-user" \
    -e MYSQL_PASSWORD="mysql-pass" \
    -v $curDir/docker-volumes/mysql:/var/lib/mysql \
    -p 3306:3306 \
    mysql:latest
