#docker-compose -f wordpress-mysql-phpmyadmin.yml up -d
curDir=$(pwd)
docker run --name wordpress \
  --restart unless-stopped \
  --network my-bridge \
	-p 2020:80 \
	-v ${curDir}/docker-volumes/wordpress:/var/www/html \
	-e WORDPRESS_DB_HOST="mysql" \
	-e WORDPRESS_DB_USER="mysql-user" \
	-e WORDPRESS_DB_PASSWORD="mysql-pass" \
	-e WORDPRESS_DB_NAME="my-db" \
	-d  wordpress:php7.4-apache
